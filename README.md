#  Material

## For the case

- 8mm Plywood
- [WaveShare 10.1" Display](https://www.amazon.de/-/en/gp/product/B01M7RRBUT/ref=ppx_yo_dt_b_search_asin_title?ie=UTF8&psc=1) 
- Raspberry Pi (optional)

## For the RFID Reader (2log Dot)

- PN523 RFID reader (same which is used for the 2log Dot)
- PLA Filament for the RFID reader case
- Translucent Plexiglass (I recommend [BLACK & WHITE 9H04 SC](https://www.plexiglas-shop.com/en-de/products/plexiglas-led/pl9h04-sc-3-00-3050x2030-b-01-s.html?listtype=search&searchparam=PLEXIGLAS®%20LED%2C%20Black%20%26%20White%209H04%20SC) )

# Tools

- CNC Mill (e.g. a [MPCNC](https://docs.v1engineering.com/mpcnc/intro/) )
- 3D Printer to print the reader case 
- Lasercutter to cut out the front glass of the RFID reader and the foam rubber plate for the support surface of the display. You may also be able to do this with a plotter.

# Steps

Mill the plywood parts and glue them together with wood glue (except for the back, which is held by itself). Print and laser the parts for the dot, assemble it and put it in the hole of the front. The layer of foam rubber between the front and the display is optional, but it helps to create a seamless transition between the wood and the display.


![](/images/1_milling.jpeg)

![](/images/2_plywood_parts.jpeg)

![](/images/4_sponge_rubber_behind_display.jpeg)

![](/images/5_step_A.jpeg)

![](/images/6_step_B.jpeg)

![](/images/7_step_C.jpeg)

![](/images/9_paint.jpeg)
